/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author matheus
 */
public class Populacao {

    private ArrayList<Individuo> populacao;
    private Random rn;
    private Scanner sc;

    public Populacao() {
        this.rn = new Random();
        this.populacao = new ArrayList<>();
        this.sc= new Scanner(System.in);
    }

    public Populacao(ArrayList<Individuo> populacao) {
        this.rn = new Random();
        this.populacao = populacao;
    }

    public void gerarPopulacao() {

        ArrayList genes = new ArrayList();

        char sinal;
        int numero;
        
       // populacao.add(informarElemento());
        
        for (int i = 0; i < 5; i++) {

            String bits = "";
            for (int c = 0; c < 5; c++) {
                numero = rn.nextInt(2);
                bits = bits + numero;
                genes.add(numero);
            }
            sinal = bits.charAt(0);
            bits = bits.substring(1);
            numero = Integer.parseInt(bits, 2);
            if (sinal == '1') {
                numero *= -1;
            }

            Individuo obj = new Individuo(numero, genes);
            populacao.add(obj);
        }
    }

    public Individuo selecionarIndividuo() {

        Individuo individuo1, individuo2, retorno;
        int resul1, resul2;

        individuo1 = this.populacao.get(rn.nextInt(4));
        individuo2 = this.populacao.get(rn.nextInt(4));

        resul1 = (individuo1.getNumero() * individuo1.getNumero()) - (3 * individuo1.getNumero()) + 4;
        resul2 = (individuo2.getNumero() * individuo2.getNumero()) - (3 * individuo2.getNumero()) + 4;

        if (resul1 < resul2) {
            retorno = individuo1;
        } else {
            retorno = individuo2;
        }

        return retorno;
    }

    public int recuperarMelhorIndividuo() {

        Individuo retorno, individuo2;
        int resul1, resul2;

        retorno = this.populacao.get(0);
        resul1 = (retorno.getNumero() * retorno.getNumero()) - (3 * retorno.getNumero()) + 4;

        for (int i = 1; i < this.populacao.size(); i++) {

            individuo2 = this.populacao.get(i);
            resul2 = (individuo2.getNumero() * individuo2.getNumero()) - (3 * individuo2.getNumero()) + 4;

            if (!(resul1 < resul2)) {
                resul1 = resul2;
                retorno = individuo2;
            }

        }

        return retorno.getNumero();
    }
    
    private Individuo informarElemento(){
        
        ArrayList bits = new ArrayList();
        int numero;
        String binario,sinal;
        
        System.out.println("Informe um elemento: ");
        numero =   -1 ; //sc.nextInt();
        binario = Integer.toBinaryString(numero);
        
        if(numero < 0)
            bits.add(1);
            else
            bits.add(0);
        
        for(int c = 0; c < binario.length(); c++){
            bits.add(Integer.parseInt(""+binario.charAt(c)));
        }
       
        Individuo ind = new Individuo(numero, bits);
        
       return ind;   
    }
}
