/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author matheus
 */
public class Mundo {
    
    private int nivel;
    private int valor;
    int teste = 0;
    private ArrayList<Populacao> mundo;
    private Random rn;
    private Populacao populacao;
    
    public Mundo() {
        this.valor= 50;
        this.populacao = new Populacao();
        this.rn = new Random();
        this.mundo = new ArrayList<Populacao>();
    }
    public Populacao gerarPrimeiraPopulacao() {
        this.populacao.gerarPopulacao();
        this.mundo.add(populacao);
        return this.populacao;
    }
    public Populacao cruzamento(Populacao populacao) {
        ArrayList<Individuo> individuos = new ArrayList<>();
        while (individuos.size() < 5) {
            Individuo paiA = populacao.selecionarIndividuo();
            Individuo paiB = populacao.selecionarIndividuo();
            ArrayList DNAA = new ArrayList();
            ArrayList DNAB = new ArrayList();
            int dnaa, dnab;
            String geneA = "", geneB = "";
            for (int c = 0; c < 5; c++) {
                if (c < 2) {
                    // t == 0 ? 10 : 20
                    dnaa = mutacao(rn.nextInt(100) + 1, (int) paiA.getGenotipo().get(c));
                    DNAA.add(dnaa);
                    geneA = geneA + dnaa;
                    dnab = mutacao(rn.nextInt(100) + 1, (int) paiB.getGenotipo().get(c));
                    DNAB.add(dnab);
                    geneB = geneB + dnab;
                } else {
                    dnaa = mutacao(rn.nextInt(100) + 1, (int) paiB.getGenotipo().get(c));
                    DNAA.add(dnaa);
                    geneA = geneA + dnaa;
                    dnab = mutacao(rn.nextInt(100) + 1, (int) paiA.getGenotipo().get(c));
                    DNAB.add(dnab);
                    geneB = geneB + dnab;
                }
            }
            char bitA, bitB;
            int valorA, valorB;
            bitA = geneA.charAt(0);
            bitB = geneB.charAt(0);
            geneA = geneA.substring(1);
            geneB = geneB.substring(1);
            valorA = Integer.parseInt(geneA, 2);
            valorB = Integer.parseInt(geneB, 2);
            if (bitA == '1') {
                valorA *= -1;
            } else if (bitB == '1') {
                valorB *= -1;
            }
            if ((valorA >= -10) && (valorA <= 10) && (individuos.size() < 5)) {
                Individuo A = new Individuo(valorA, DNAA);
                individuos.add(A);
            }
            if ((valorB >= -10) && (valorB <= 10) && (individuos.size() < 5)) {
                Individuo B = new Individuo(valorB, DNAB);
                individuos.add(B);
            }
        }
        Populacao pop = new Populacao(individuos);
        this.mundo.add(pop);
        this.mundo.remove(0);
        return pop;
    }
    
    private int mutacao(int mutacao, int gene) {
        if (mutacao == 1) {
            if (gene == 1) {
                gene = 0;
            } else {
                gene = 1;
            }
        }
        return gene;
    }
    
    public void melhorResultado(int geracao) {
  
        Populacao pop = mundo.get(0);
         
       if (pop.recuperarMelhorIndividuo() < valor) {
                valor = pop.recuperarMelhorIndividuo();
                nivel = geracao;
            }

    }

    public int getNivel() {
        return nivel;
    }

    public int getValor() {
        return valor;
    }
    
}
