/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio;

/**
 *
 * @author matheus
 */
public class Exercicio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Populacao primeira;
        Mundo mundo = new Mundo();
       primeira = mundo.gerarPrimeiraPopulacao();
       
       for(int i = 0; i < 100; i++){
           primeira = mundo.cruzamento(primeira);
           mundo.melhorResultado(i);
       }
        System.out.println("Melhor individuo: "+mundo.getValor());
        System.out.println("População: "+(mundo.getNivel() + 1));
    }

}
