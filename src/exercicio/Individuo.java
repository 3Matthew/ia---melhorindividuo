/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio;

import java.util.ArrayList;

/**
 *
 * @author matheus
 */
public class Individuo {

    private int numero;
    private ArrayList genotipo;

    public Individuo() {

    }

    public Individuo(int numero, ArrayList genotipo) {
        this.numero = numero;
        this.genotipo = genotipo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public ArrayList getGenotipo() {
        return genotipo;
    }

    public void setGenotipo(ArrayList genotipo) {
        this.genotipo = genotipo;
    }
}
